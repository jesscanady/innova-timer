class ApplicationController < ActionController::Base

  private

  def set_date
    @date = if params[:date].present?
              begin 
                Date.parse(params[:date])
              rescue ArgumentError
                Date.today
              end
            else
              Date.today
            end
    @yesterday = @date - 1.day
    @tomorrow  = @date + 1.day
    @today     = Date.today
  end

end
