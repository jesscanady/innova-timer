class AdminController < ApplicationController
  before_action :redirect_if_not_admin

  private

  def redirect_if_not_admin
    redirect_to root_url, alert: "Sorry, your name's not on the list." unless current_user.admin?
  end
end
