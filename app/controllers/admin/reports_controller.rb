require "client_report"

class Admin::ReportsController < AdminController
  before_action :set_header_and_page

  def index
    @clients = Client.ordered.all
  end

  def create
    @report_client_id  = params[:report_client_id]
    @report_start_date = params[:report_start_date]
    @report_end_date   = params[:report_end_date]

    report = ClientReport.new(
      client_id:  params[:report_client_id],
      start_date: params[:report_start_date],
      end_date:   params[:report_end_date])

    respond_to do |format| 
      format.html do
        @report_data = report.generate
      end
      format.csv do
        send_data report.csv, filename: "report.csv", type: "text/csv",
                  disposition: :attachment
      end
    end
  end

  private

  def set_header_and_page
    @header_text = "Reports"
    @page        = :reports
  end
end

