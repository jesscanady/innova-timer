class Project < ApplicationRecord
  belongs_to :client
  scope :ordered, -> { order(name: :asc) }

  delegate :name, to: :client, prefix: true
end
