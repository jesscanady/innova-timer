require "gravatar"

module ApplicationHelper
  def current_user_gravatar_url
    Gravatar.new(current_user.email).image_url
  end
end
