# Innova Timer Project Specification

We want an application to help our consultants track their time for customer projects.

- they may not start timers except on the current day

- Reporting UI must be good -- no text entry of dates.

- Admins can assign projects to users so they can only see their assigned projects.

- Sign in page should look like someone cared, even a little.
 
- Users can change their passwords.

- Multi Tenancy -- more than one account.

- Factor out the date formats we've repeated everywhere in the code.

- Make CTRL-RET submit the form.

- Validation on the report form.


## Completed Features

- When an employee logs in, they should be able to select a project and enter how long they've worked on that project.

- The employee may enter the number of hours worked, or start a timer that will later be stopped.

- Each project will be assigned to a client, but a client may have multiple projects that will be billed separately.

- The employee may also navigate to previous days and enter time for them.
 
- Admins can run reports for a given client over a given time period.

- If an employee closes the application with a timer running, the timer should remain running until the employee explicitly stops it. If possible, send a reminder email at the end of each day if there are active timers.

- UI should show who is currently logged in (name).

- We should enforce that only admins can access the /admin/reports page.

- Gravatar support -- we know folks' email addresses, we can pull better profile pics.

- Reports can be exported to CSV (Excel)
