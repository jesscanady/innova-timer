require "rails_helper"

RSpec.describe Entry do
  it "can find entries for a given date" do
    date = '2020-04-01'
    FactoryBot.create(:entry, recorded_date: date)

    expect(Entry.for_date(date).count).to eq(1)
  end

  it "sets the start_time column if no duration is present during creation" do
    entry = FactoryBot.build(:entry, project: FactoryBot.create(:project), 
                             duration: nil, start_time: nil)
    entry.save
    expect(entry.start_time).to be_present
  end

  it "does not set start_time if duration is present" do
    entry = FactoryBot.build(:entry, project: FactoryBot.create(:project), 
                             duration: 3, start_time: nil)
    entry.save
    expect(entry.start_time).to be_nil  
  end

  it "does not set start_time if start_time is present" do
    start_time = 2.hours.ago
    entry = FactoryBot.build(:entry, project: FactoryBot.create(:project), 
                             duration: nil, start_time: start_time)
    entry.save
    expect(entry.start_time.to_s).to eq(start_time.to_s)  
  end

  it "is a running timer if the duration is nil but the start_time is present" do
    entry = FactoryBot.build(:entry, duration: nil, start_time: Time.now)
    expect(entry).to be_running_timer
  end

  it "knows how to stop a running timer" do
    entry = FactoryBot.create(:entry, duration: nil, start_time: 2.hours.ago)
    entry.stop_timer!
    expect(entry.duration).to eq(2.0)
    expect(entry).to be_persisted
  end
end