require "rails_helper"
require "gravatar"

RSpec.describe Gravatar do
  it "constructs gravatar URLs" do
    gravatar = Gravatar.new("example@example.com")
    expect(gravatar.image_url).to start_with("https://www.gravatar.com/avatar/")
  end

  it "generates different URLs for different inputs" do
    gravatar1 = Gravatar.new("example@example.com")
    gravatar2 = Gravatar.new("different@example.com")

    expect(gravatar1.image_url).not_to eq(gravatar2.image_url)
  end

  it "strips whitespace and downcases when generating hashes" do
    known_good_md5 = Digest::MD5.hexdigest("testdata")
    bad_input      = "     TESTdata    "
    gravatar       = Gravatar.new(bad_input)

    expect(gravatar.email_hash).to eq(known_good_md5)
  end
end
