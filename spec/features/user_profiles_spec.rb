require "rails_helper"

RSpec.feature "User profiles" do
  scenario "can be updated by users", js: true do
    login_as FactoryBot.create(:user)

    visit "/"

    click_on "user-menu"
    click_on "Your Profile"

    fill_in "user_name", with: "My New Name"
    click_on "Update profile"

    within('nav') do
      expect(page).to have_content "My New Name"
    end
  end
end
