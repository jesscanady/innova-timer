require "rails_helper"

RSpec.feature "Tracking time with timers" do
  let(:user) { FactoryBot.create :user }
  let!(:project) { FactoryBot.create :project }

  before(:each) do
    login_as user
  end

  scenario "starting a timer", js: true do
    visit "/timers" 
    notes = "Some quality notes"

    select project.name, from: "entry_project_id"
    fill_in "entry_notes", with: notes
    click_on 'Start timer'

    within(".project") do
      expect(page).to have_button "Stop timer"
    end
  end

  scenario "stopping a timer" do
    FactoryBot.create :running_timer, user: user, start_time: 2.hours.ago, recorded_date: Date.today

    visit "/timers"

    within(".project") do
      click_button "Stop timer" 
      expect(page).to have_content("2.0 hours")
    end
  end
end
