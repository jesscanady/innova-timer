require "rails_helper"

RSpec.feature "Admin users" do
  let(:admin_user)     { FactoryBot.create :admin_user }
  let(:non_admin_user) { FactoryBot.create :user }

  scenario "should see the Reports link in the navbar" do
    login_as admin_user

    visit "/"

    within('nav') do
      expect(page).to have_link('Reports')
    end
  end

  scenario "do not see Reports link if not logged in as admins" do
    login_as non_admin_user

    visit "/"

    within('nav') do
      expect(page).not_to have_link('Reports')
    end
  end
end
