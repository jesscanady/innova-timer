require "rails_helper"

RSpec.feature "Admins: Generating Reports" do
  let!(:client)        { FactoryBot.create :client }
  let(:non_admin_user) { FactoryBot.create :user }
  let(:admin_user)     { FactoryBot.create :admin_user }

  scenario "non-admins cannot generate reports" do
    login_as non_admin_user

    visit "/"
    within("nav") do
      expect(page).not_to have_link("Reports")
    end

    visit "/admin/reports"
    # We really want to ensure /admin/reports is a redirect
    expect(page).to have_content "Track your time"
  end

  scenario "can generate reports" do
    login_as admin_user

    visit "/admin/reports/"

    fill_in "report_start_date", with: "2020-04-01"
    fill_in "report_end_date", with: "2020-04-30"
    select client.name, from: "report_client_id"

    click_button "View report"

    expect(page).to have_content("Report Results")
  end

  scenario "can download reports as CSV" do
    login_as admin_user

    visit "/admin/reports/"

    fill_in "report_start_date", with: "2020-04-01"
    fill_in "report_end_date", with: "2020-04-30"
    select client.name, from: "report_client_id"

    click_button "View report"

    click_link "Download report"

    expect(page.response_headers['Content-Disposition']).to match(/^attachment/)
    expect(page).to have_content("project,employee,date,duration,notes")
  end
end
